from django import forms
import datetime

class Message_Form(forms.Form):
    description = forms.CharField(label='activity', required=True, max_length=300, widget=forms.TextInput(attrs = {"class": "form-control"}))