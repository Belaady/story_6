from django.test import TestCase
from django.test import Client, LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import *
from .forms import *
from django.urls import resolve
from selenium import webdriver
import time
import unittest

# Create your tests here.
class s6UnitTest(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    def test_form_status_html(self):
        form = Message_Form()
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_view_s6(self):
        response = self.client.post('/', 
            data={'description' : 'Coba-Coba'})
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)
        found = resolve('/')
        self.assertEqual(found.func, home)
        

    def test_model_Status(self): 
        record = Status.objects.create(description="happy", date="2000-09-07")
        count_records = Status.objects.all().count()
        self.assertEqual(count_records, 1)


class FunctionalTest(LiveServerTestCase):
    def setUp(self): 
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrive_it_later(self):
        self.browser.get(self.live_server_url+'/')
        self.assertIn('Halo apa kabar?', self.browser.page_source)
        time.sleep(1)

