from django.shortcuts import render
from datetime import datetime, date
from .forms import *
from .models import *

# Create your views here.

def home(request):
    form = Message_Form()
    status = Status.objects.all()
    response = {'message_form': form, 'mystatus' : status}
    if request.method == 'POST':
        description = request.POST['description']
        status = Status(description=description,date=datetime.datetime.now())
        status.save()
    return render(request, 'home.html', response)

